/* user and group to drop privileges to */
static const char *user  = "kellyr";
static const char *group = "kellyr";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "green",     /* after initialization */
	[INPUT] =  "#005577",   /* during input */
	[FAILED] = "#CC3333",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* time in seconds before the monitor shuts down */
extern const int monitortime = 5;
/* default message */
extern const char * message = "Suckless: Software that sucks less.";

/* text color */
extern const char * text_color = "#ffffff";

/* text size (must be a valid size) */
extern const char * font_name = "-bitstream-bitstream vera sans mono-bold-o-normal--0-0-0-0-m-0-iso8859-1";
